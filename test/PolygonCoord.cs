﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test
{
        public class InsidePolyCoord
        {
            public double lat { get; set; }
            public double lng { get; set; }

        }

        public class PolyCoord
        {
            public double lat { get; set; }
            public double lng { get; set; }

        }

        public class PolygonsResult
        {
            public string polygon_id { get; set; }
            public List<InsidePolyCoord> insidePolyCoords { get; set; }
            public List<PolyCoord> polyCoords { get; set; }

        }

        public class Root
        {
            public List<PolygonsResult> polygonsResult { get; set; }

        }
}
