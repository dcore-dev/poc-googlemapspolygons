﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace test.Controllers
{
    [Route("api")]
    [ApiController]
    public class ValuesController1 : ControllerBase
    {
        private readonly IWebHostEnvironment WebHostEnvironment;
        public ValuesController1(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }

        // POST api/<ValuesController1>
        [HttpPost]
        [Route("post")]
        public IActionResult jsonPostToFile([FromBody] Root value)
        {
            JObject obj = (JObject)JToken.FromObject(value);
            CreateFile(obj);
            return Ok("Archivo creado");
        }

        //Create a json file with the json object post
        private void CreateFile(JObject obj)
        {
            string uploadDir = Path.Combine(WebHostEnvironment.WebRootPath, "json");
            var filename = "polygonsCoords.json";
            string filePath = Path.Combine(uploadDir, filename);
            System.IO.File.WriteAllText(filePath, obj.ToString());
        }
    }
}
